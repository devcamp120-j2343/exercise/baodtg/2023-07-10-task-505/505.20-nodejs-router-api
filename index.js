//Import thư viện expressjs
const express = require('express');

//khởi tạo 1 app express
const app = express();

//KHai báo cổng chạy project
const port = 8000;

//Callback function là 1 function đóng vai trò là tham số của 1 function khác, nó sẽ được thực hiện khi function chủ được gọi
//khai báo API dạng /
app.get("/api", (req, res) => {
    let today = new Date();
    res.json({
        message: `Xin chào hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`

    })
})

//Khai báo api dạng GET
app.get("/get-method", (req,res) => {
    res.json({
        message: `GET Method`
    })
})

//Khai báo api dạng POST
app.get("/post-method", (req,res) => {
    res.json({
        message: `POST Method`
    })
})

//Khai báo api dạng PUT
app.get("/put-method", (req,res) => {
    res.json({
        message: `PUT Method`
    })
})

//Khai báo api dạng DELETE
app.get("/delete-method", (req,res) => {
    res.json({
        message: `DELETE Method`
    })
})
app.listen(port, () => {
    console.log("App listening on port: ", port)
})